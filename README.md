# vue-crsl
Try it out here:
[Vue Carousel](https://codesandbox.io/s/vkwqmm577)
## 🚀 Usage
```bash
npm i vue-crsl
```

### Vue
```javascript
import Carousel from 'vue-crsl'
export default {
  components: {
    Carousel
  }
}
```
### Nuxt
```javascript
// Create a plugin - ~/plugins/vue-crsl.js
import Vue from 'vue'
import Carousel from 'vue-crsl'

Vue.component('Carousel', Carousel)

// nuxt.config.js
plugins: [
  {src: '~/plugins/crsl.js', ssr: false}
]
```
## Slot
**Custom arrows**
```html
<Carousel ref="carousel">
  <img src="...">
  <img src="...">
  <img src="...">
  <template v-slot:arrows>
    <button @click="$refs.carousel.prev()" :disabled="$refs.carousel.arrowsDisabled || $refs.carousel.isArrowDisabled.prev">Prev</button>
    <button @click="$refs.carousel.next()" :disabled="$refs.carousel.arrowsDisabled || $refs.carousel.isArrowDisabled.next">Next</button>
  </template>
</Carousel>
```
**Custom dots**
```html
<Carousel ref="carousel">
  <img src="...">
  <img src="...">
  <img src="...">
  <template v-slot:dots="{activeIndex, items}">
    {{ activeIndex }}
  </template>
</Carousel>
```

## Props
| property | default |
| ------ | ------ |
| dots | true |
| arrows | true |
| infinite | true |
| transitionDuration | 500 |
| adaptiveHeight | false |
| autoplay | 3000 |
| autoplayDirection | forward |
| pauseOnHover | true |
| lazyLoad | false |
| loadingBar | true |
| loadingBarPercentage | true |
| fade | false |
| dragOffset | 50 |


## Events
| event | return |
| ------ | ------ |
| init | instance |
| change | index, activeElement |
| before-change | oldIndex, index, activeElement |
| after-change | index, oldIndex, activeElement |
| navigation-clicked | index, oldIndex, activeElement |
| arrow-clicked | index, oldIndex, direction |
| dot-clicked | index, oldIndex, activeElement |
| drag | instance |
| dragstart | instance |
| dragend | instance |

## Methods
| method | parameters |
| ------ | ------ |
| goToSlide | index |
| append | DOM element |
| prepend | DOM element |
| next | - |
| prev | - |